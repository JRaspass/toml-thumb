# TOML::Thumb

[![pipeline]](https://gitlab.com/JRaspass/toml-thumb/commits/master)

In English folklore, [Tom Thumb] was a diminutive man. TOML::Thumb is a
diminutive TOML (de)serializer.

It aims to fully support [TOML v1.0.0].

**Note until the full test suite passes this module will remain pre 1.0 and
breaking changes are to be expected.**

## SYNOPSIS

```raku
use TOML::Thumb;

my %config = 'config.toml'.IO.&from-toml;

spurt 'config.toml', to-toml %config;
```

## SUBROUTINES

### from-toml

```raku
multi from-toml(IO  $toml)
multi from-toml(Str $toml)
```

### to-toml

```raku
sub to-toml(%hash)
```

## PERFORMANCE

Testing the large TOML file from [CPAN Rocks].

```
$ ls -hs dists.toml; wc -l $_
4.1M dists.toml
282687 dists.toml
```

```
$ time raku -MTOML::Thumb -e 'from-toml slurp "dists.toml"'

real    0m15.926s
user    0m15.814s
sys     0m0.210s

$ time raku -MConfig::TOML -e 'from-toml slurp "dists.toml"'

real    186m51.735s
user    186m22.130s
sys     0m11.225s
```
```
$ raku -v
This is Rakudo version 2020.02 built on MoarVM version 2020.02
implementing Raku 6.d.

$ lscpu | sed -nr '/Model name/ s/.*: +//p'
Intel(R) Core(TM) i7-8700 CPU @ 3.20GHz
```

## SEE ALSO

- [Config::TOML](https://raku.land/github:atweiden/Config::TOML)
- [TOML](https://raku.land/zef:tony-o/TOML)

[cpan rocks]:  https://cpan.rocks
[pipeline]:    https://gitlab.com/JRaspass/toml-thumb/badges/master/pipeline.svg
[tom thumb]:   https://en.wikipedia.org/wiki/Tom_Thumb
[toml v1.0.0]: https://toml.io
