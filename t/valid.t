use TOML::Thumb;
use Test;

my $todo = set <
    comment/tricky
    comment/tricky-round-trip
    float/exponent
    float/exponent-round-trip
    float/underscore
    float/underscore-round-trip
    float/zero
    float/zero-round-trip
    inline-table/key-dotted
    inline-table/key-dotted-round-trip
    key/escapes-round-trip
    string/escape-tricky
    string/escape-tricky-round-trip
    string/multiline
    string/multiline-escaped-crlf
    string/multiline-escaped-crlf-round-trip
    string/multiline-quotes
    string/multiline-quotes-round-trip
    string/multiline-round-trip
>;

for sort 't/valid'.IO.map: { .d ?? |.dir».&?BLOCK !! $_ } -> $json, $toml {
    my $name = $toml.extension('').subst: /^ 't/valid/' /;

    todo 'not yet implemented' if $todo{$name};

    my $got = try from-toml $toml;
    my $exp = Rakudo::Internals::JSON.from-json: $json.slurp;

    is-deeply walk($got), $exp, $name;

    todo 'not yet implemented' if $todo{ $name ~= '-round-trip' };

    is-deeply try { walk from-toml to-toml $got }, $exp, $name;
}

multi walk(Any     $value) { $value }
multi walk(Array   $value) { $value».&walk }
multi walk(Bool    $value) { { :type<bool>,    :value($value.lc) } }
multi walk(Hash    $value) { %( $value.map: { .key => walk(.value) } ) }
multi walk(Int     $value) { { :type<integer>, :value(~$value) } }
multi walk(Real    $value) { { :type<float>,   :value($value.lc) } }
multi walk(Str     $value) { { :type<string>,  :$value } }
multi walk(Dateish $value) { {
    type  => $value.^name.subst('TOML::Thumb::').subst('::', '-').lc,
    value => ~$value,
} }

done-testing;
