use TOML::Thumb;
use Test;

my $todo = set <
    array/tables-1
    inline-table/add
    inline-table/linebreak-1
    inline-table/linebreak-2
    inline-table/linebreak-3
    inline-table/linebreak-4
    key/after-array
    key/after-table
    key/after-value
    key/no-eol
    table/append-with-dotted-keys-1
    table/append-with-dotted-keys-2
    table/duplicate
    table/duplicate-key-dotted-table
    table/duplicate-key-dotted-table2
    table/injection-1
    table/injection-2
>;

my %throws = (
    'inline-table/duplicate-key' => 'Duplicate key ｢b｣ in inline table';
);

for sort 't/invalid'.IO.map: { .d ?? |.dir».&?BLOCK !! $_ } {
    my $name = .extension('').subst: /^ 't/invalid/' /;

    todo 'not yet implemented' if $todo{$name};

    if %throws{$name}:exists {
        throws-like { .&from-toml }, Exception, message => %throws{$name};
    }
    else {
        is-deeply try { .&from-toml }, Nil, $name;
    }
}

done-testing;
